 #  Bobble Content SDK

Bobble Content SDK provides an all in one solution for your content requirements:
1. Creating Bobble Heads (Cartoonised Avatar heads from user's selfie)
2. Head management - Add/Delete/Change Heads
3. Stickers & Animated Stickers

Convert your user's selfies into fun personalised avatars <i>(Bobble Head)</i> and use it with large repository of personalised and expressive content(100K+).

## <a name="implementation_steps"></a>Implementation Steps

- Add and initialise BobbleSDK Core in your project. Refer [here](README.md#setup) for steps.

- Add following dependency in your application module’s build.gradle.
```groovy
implementation 'com.touchtalent.bobblesdk:content'
```

Sync your Gradle project to ensure that the dependency is downloaded by the build system.

## Required Permissions : 

```xml
<uses-permission android:name="android.permission.CAMERA" /> // For head creation
```
##  Bobble Content APIs:

### BobbleContentView
|                Dark mode                 |                Light mode                 |
| :--------------------------------------: | :---------------------------------------: |
| ![](assets/bobble-content-view-dark.jpg) | ![](assets/bobble-content-view-light.jpg) |

```BobbleContentView``` imports a complete view showing different formats of content (Sticker, Animated Sticker, PopText). The UI supports light and dark modes. The interaction with the content can be captured via listeners. The view handles multiple functionalities:

- Display content and capture user interactions with them.
- Bobble Head creation and management.
- Sticker Search

#### i. Add custom view inside a XML layout of your Keyboard layout
```xml
<com.touchtalent.bobble.content.BobbleContentView
    android:id="@+id/content_view"
    android:layout_width="match_parent"
    android:layout_height="match_parent" />
```
#### ii. APIs
1. `load(supportedMimeTypes:List<String>, currentText:String?)` - Call this function to start content loading. Following data is required for content to be loaded:
   - **supportedMimeTypes** - List of supported MIME types (extracted from `EditorInfo`). This is used to decide the type of content supported by the parent app.
   - **currentText** - Current text present in the input box. This is used to search for relevant content
                
2. `setShareListener(listener:(uri:Uri, mime:String?) -> Unit)` - Set listener to listen for content sharing. URI and mime type are provided.Uri is a `content://` URI created with FileProvider with necessary permissions for sharing.

3. `setSearchListener(onViewCreatedListener: (View, EditText) -> Unit, onRevertToStickerView: () -> Unit)` - Since, keyboard input, inside a keyboard itself, is a complicated mechanism, these callbacks facilitate taking input for the search functionality in content panel.
    - **onViewCreatedListener** - Called with a custom view - *searchView* (containing search suggestions, need to be added on top of keyboard) and EditText - editText - which should be used to transfer keyboard's focus to receive inputs. Upon receiving this callback, the keyboard view must be restored and content panel view must be cached.
    - **onRevertToStickerView** - Called when back button is pressed on searchView or search action button is pressed. Upon receiving this callback, the content panel view must be restored.
4. `canDestroy:Boolean` - Use this flag to prevent auto-destroy of BobbleContentView. This is done to prevent clearing out resources when keyboard layout is shown, upon content search, since same instance must be retained to show search results.
   >P.S:- The view must be carefully stored and `destroy()` called to clear out resources and prevent memory leak
5. `destroy()` - Use this function to clear and destroy resources and memory held by this view. By default, the view clears itself and there is no need to call this. However, if `canDestroy` is set `false`, it is mandatory to call this function. Failing to do so can lead to memory leaks.

iii. Example

- sample_content_panel.xml

```xml
<?xml version="1.0" encoding="utf-8"?> 
<androidx.constraintlayout.widget.ConstraintLayout 
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:background="@color/white">

    <com.touchtalent.bobble.content.BobbleContentView
        android:id="@+id/content_view"
        android:layout_width="match_parent"
        android:layout_height="match_parent" />
</androidx.constraintlayout.widget.ConstraintLayout>
```
- openContentPanel()
```kotlin
    fun openContentPanel(){
        val contentPanel = SampleContentPanelBinding.inflate(layoutInflater).also {
            sampleContentPanelBinding = it // Cache this instance
            val contentView: BobbleContentView = it.contentView
            contentView.canDestroy = false
            contentView.load(
                supportedMimeTypes,
                getCurrentText(),
                currentInputEditorInfo?.packageName ?: ""
            )
            contentView.setSearchListener(
                onViewCreatedListener = { searchView, editText ->
                    removeCustomView(false) // Remove bobbleContentView with keyboard view for user input
                    addTopView(searchView) // Add this view above keyboardview for search suggestions
                    changeInputConnection(editText) // Bind keyboard connection to this EditText
                },
                onRevertToStickerView = {
                    // Revert cached instance of BobbleContentView back to screen
                    sampleContentPanelBinding?.root?.let { contentView ->
                        addCustomView(contentView)
                    }
                }
            )
            contentView.setShareListener { uri, mime ->
                shareUri(uri, mime)
            }
        }
    }

    private fun removeCustomView(canDestroy: Boolean) {
        resetCustomInputConnection()
        val inputViewBinding = inputViewBinding ?: return
        inputViewBinding.customViewContainer.isVisible = false
        inputViewBinding.customViewContainer.removeAllViews()
        if (canDestroy) {
            sampleContentPanelBinding?.contentView?.destroy()
            sampleContentPanelBinding = null
        }
    }

```