# BobbleSDK

BobbleSDK is a collection of multiple SDKs which can be used to power conversations. BobbleSDK is designed to provide tailored experience to suite requirements for applications and IMEs. 

## <a name="requirements"></a>System Requirements
- Minimum supported Android SDK level is 23 (Android 6.0)
- System architechture (ABIs) supported are [arm64-v8a, armeabi-v7a]

## Modules
BobbleSDK consists of multiple modules, `core` module being the base for all of them. This guide walks you through setting up `core` module in your application. Please go through the seperate guides for integrating feature specific modules

- [Bobble Content Panel (:content)](content.md) - This module which exposes a `BobbleContentView` offering shareable Stickers and Animated stickers in a 3X3 grid format across multiple categories.
- [Bobble Content Drawer (:content-drawer)](content-drawer.md) - This module which exposes a `BobbleContentDrawerView` offering shareable Stickers, PopText, Bigmojis, Animated stickers and Regional GIFs in a horizontal scrollable view.
- [Bobble Auto Prompt (:auto-prompt)](auto-prompt.md) - This module which provides realtime content suggestions based on user's typed text. The suggestions driven can be fulfilled by `BobbleContentDrawerView`


### <a name="setup"></a>Setting Up

- Setup Maven in `settings.gradle` (`build.gradle`(project level) for older versions of Android Gradle Plugin).
```groovy
maven {
    url "<mavenReadUrl>" // Credentials for maven repo
    credentials {
        username "<mavenReadUsername>"
        password "<mavenReadPassword>"
    }
}
```
- Make sure that `jcenter()` exists in the repository list, since our libraries depends on libraries hosted on `jcenter()`

- BobbleSDK uses BoM (Bill of Materials) to resolve versions of all modules by specifying a single version. Import the BoM for the BobbleSDK platform by adding following dependency in your application module’s build.gradle. 
```groovy
implementation platform('com.touchtalent.bobblesdk:bom:poc-1.0.0')
```

- Import BobbleSDK Core which acts as base for all modules.
```groovy
implementation 'com.touchtalent.bobblesdk:core'
```

- Initialise BobbleSDK in your Application's ```onCreate()```
```kotlin
class SampleDemoApp : Application() {
    override fun onCreate() {
        super.onCreate()
        BobbleSDK.initialise(this)
    }
}
```

### <a name="binding"></a> Binding to BobbleCore
Certain data points and hooks need to be provided to Bobble Core. Please ensure all following callbacks and methods are integrated for all features to ensure smooth feature integrations
- **User Age and Gender** - Provide user Age and Gender details to BobbleSDK. These information will be used to personalise the content that is shown to the user. These functions can be invoked whenever age and gender is available.
```kotlin
BobbleSDK.setUserAge(34)
BobbleSDK.setUserGender(BobbleSDK.Gender.MALE)
```
- **User langauges** - We need language information w.r.t IME to be able to personalise the content and other offerings. Locales of user downloaded langauges and current keyboard locale needs to be provided. These functions need to be called everytime user changes their language or downloads/deletes a language.
```kotlin
// Pass locales of all languages that the user has downloaded. This helps us to download
// required models and metadata for personalisation of content and intent detection from
// typed text
BobbleSDK.setCurrentDownloadedLocales(listOf("en_IN", "hi_IN", "ko_KR"))
// Pass locale of current keyboard language. This is responsible for controlling language
// of all features of our SDK
BobbleSDK.setCurrentLocale("en_IN")
```

- **BobbleInputMethodDelegate** - Create `BobbleInputMethodDelegate` to access services (like `auto-prompt`) to personalise content and to detect intents. Following IME callbacks needs to be provided to the module. It is preferred to call these methods right after super calls. 

    These callbacks are used to bind to the IME lifecycle and facilitate personalization tools to work optimally. For e.g - `onStartInputView` lets us know which app keyboard is open and whether or not content drawer should be opened here. (Content Drawer is opened only on messaging apps)

```kotlin
class SampleKeyboard : InputMethodService() {
    
    private var bobbleDelegate: BobbleInputMethodDelegate =
        BobbleSDK.newInputMethodServiceDelegate(this)

    override fun onCreate() {
        super.onCreate()
        bobbleDelegate.onCreate()
    }

    override fun onStartInput(attribute: EditorInfo?, restarting: Boolean) {
        super.onStartInput(attribute, restarting)
        bobbleDelegate.onStartInput(attribute, restarting)
    }

    override fun onStartInputView(info: EditorInfo?, restarting: Boolean) {
        super.onStartInputView(info, restarting)
        bobbleDelegate.onStartInputView(info, restarting)
    }
    
    override fun onFinishInputView(finishingInput: Boolean) {
        super.onFinishInputView(finishingInput)
        bobbleDelegate.onFinishInputView(finishingInput)
    }

    override fun onFinishInput() {
        super.onFinishInput()
        bobbleDelegate.onFinishInput()
    }

    override fun onDestroy() {
        super.onDestroy()
        bobbleDelegate.onDestroy()
    }

    // listen on every input and pass complete user typed text to personlise other features
    fun onTextUpdate(inputText: String){
        bobbleDelegate.processText(inputText)
    }
}
```

### <a name="permissions"></a>Required Permissions

The SDK and modules uses few basic permissions, as listed below, for their smooth functioning. 

```java
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
```
Apart from the necessary permissions, the SDK recommends the client app to add following permissions and request it from their users for a customised experience.
```java
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
```

>P.S - Individual modules may require specific permissions which will be declared in their respective documentation.