package com.touchtalent.bobblesdk.sample.samsung.activity

import android.os.Bundle
import androidx.core.view.isVisible
import com.touchtalent.bobblesdk.sample.samsung.databinding.ActivityMainBinding

/**
 * Demo app to show-case usage of all BobbleSdk modules
 */
class MainActivity : BobbleEnablerActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.step1.setOnClickListener {
            startActivationFlow()
        }
        binding.step2.setOnClickListener {
            startActivationFlow()
        }
    }

    override fun onStatusChange(imeInstallStatus: IMEInstallStatus) {
        updateStatus(imeInstallStatus)
    }
    override fun onResume() {
        super.onResume()
        updateStatus(getStatus())
    }

    private fun updateStatus(status: IMEInstallStatus) {
        binding.step1.isVisible = status == IMEInstallStatus.NONE
        binding.step2.isVisible = status == IMEInstallStatus.ENABLED
        binding.keyboardEnabled.isVisible = status == IMEInstallStatus.SELECTED
    }

}