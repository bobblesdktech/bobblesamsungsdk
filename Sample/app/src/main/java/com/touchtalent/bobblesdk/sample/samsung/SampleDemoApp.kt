package com.touchtalent.bobblesdk.sample.samsung

import android.app.Application
import android.content.Context
import com.touchtalent.bobble.core.BobbleSDK

open class SampleDemoApp : Application() {
    companion object {
        lateinit var applicationContext: Context
    }

    override fun onCreate() {
        super.onCreate()
        Companion.applicationContext = this
        // Initialise BobbleSdk and all its module
        BobbleSDK.initialise(this)
    }
}