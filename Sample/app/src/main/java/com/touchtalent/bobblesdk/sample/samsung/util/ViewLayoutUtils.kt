package com.touchtalent.bobblesdk.sample.samsung.util

import android.content.Context
import android.view.View
import android.view.Window
import com.touchtalent.bobblesdk.sample.samsung.SampleDemoApp

fun updateLayoutHeightOf(window: Window, layoutHeight: Int) {
    val params = window.attributes
    if (params.height != layoutHeight) {
        params.height = layoutHeight
        window.attributes = params
    }
}

fun updateLayoutHeightOf(view: View, layoutHeight: Int) {
    val params = view.layoutParams
    if (params.height != layoutHeight) {
        params.height = layoutHeight
        view.layoutParams = params
    }
}

fun dpToPx(dp: Float, context: Context): Int {
    val scale = context.resources.displayMetrics.density
    return (dp * scale + 0.5f).toInt()
}

val Int.dp: Int
    get() = dpToPx(this.toFloat(), SampleDemoApp.applicationContext)

val Float.dp: Int
    get() = dpToPx(this, SampleDemoApp.applicationContext)