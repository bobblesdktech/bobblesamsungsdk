package com.touchtalent.bobblesdk.sample.samsung.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.view.inputmethod.InputMethodInfo
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import com.touchtalent.bobblesdk.sample.samsung.dialog.DialogUtil

/**
 * Activity to facilitate activation of SampleKeyboard
 */
open class BobbleEnablerActivity : AppCompatActivity() {

    private lateinit var checkEnabled: Runnable
    private lateinit var checkEnabledHandler: Handler
    private lateinit var imeInstallStatus: IMEInstallStatus
    private var isForeground = false
    private var isSettingsPageLaunched = false
    private var showImeChooserCalled = false
    private var firstLaunch = true
    private var requestCode = 1234
    private var shouldShowImeSwitcher = false
    private var isImeSwitcherShown = false

    enum class IMEInstallStatus {
        ENABLED, NONE, SELECTED
    }

    open fun getStatus(context: Context): IMEInstallStatus {
        val imeManager = context.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        val listOfKeyboards: List<InputMethodInfo> = imeManager.enabledInputMethodList
        for (i in listOfKeyboards) {
            if (i.packageName == context.packageName) {
                return if (i.id.equals(
                        Settings.Secure.getString(
                            context.contentResolver,
                            Settings.Secure.DEFAULT_INPUT_METHOD
                        )
                    )
                ) {
                    IMEInstallStatus.SELECTED
                } else IMEInstallStatus.ENABLED
            }
        }
        return IMEInstallStatus.NONE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        try {
            super.onCreate(savedInstanceState)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        checkEnabled = Runnable {
            val status = getStatus(this);
            if (this.imeInstallStatus != status) {
                if (status == IMEInstallStatus.ENABLED) {
                    checkEnabledHandler.removeCallbacks(checkEnabled);
                    if (!isForeground) {
                        finish()
                        val intent = Intent(this, this::class.java)
                        intent.putExtra("showImeSwitcher", true)
                        startActivity(intent)
                    }
                }
            } else checkEnabledHandler.postDelayed(checkEnabled, 300)
        }
        checkEnabledHandler = Handler(Looper.getMainLooper())
        shouldShowImeSwitcher = intent.getBooleanExtra("showImeSwitcher", false)
        if (shouldShowImeSwitcher) {
            onStatusChange(
                IMEInstallStatus.NONE,
                IMEInstallStatus.ENABLED
            )
        }
        imeInstallStatus = getStatus(this)
    }

    override fun onResume() {
        super.onResume()
        if (!firstLaunch) {
            checkEnabledHandler.removeCallbacks(checkEnabled)
        } else firstLaunch = false
        if (isSettingsPageLaunched) {
            isSettingsPageLaunched = false
            val status = getStatus(this)
            if (this.imeInstallStatus == IMEInstallStatus.NONE && status == IMEInstallStatus.ENABLED)
                showIMEChooser()
            onStatusChange(this.imeInstallStatus, status)
            this.imeInstallStatus = status
        }
        isForeground = true
    }

    override fun onPause() {
        super.onPause()
        isForeground = false
    }

    protected fun startActivationFlow() {
        checkEnabledHandler.removeCallbacks(checkEnabled)
        when (imeInstallStatus) {
            IMEInstallStatus.NONE -> {
                launchEnableKeyboardPage();
            }
            IMEInstallStatus.ENABLED -> {
                showIMEChooser()
            }
            IMEInstallStatus.SELECTED -> {
                onStatusChange(
                    IMEInstallStatus.SELECTED,
                    IMEInstallStatus.SELECTED
                )
            }
        }
    }

    protected fun getStatus(): IMEInstallStatus {
        return getStatus(this)
    }

    private fun launchEnableKeyboardPage() {
        isSettingsPageLaunched = true
        val enableIntent = Intent(Settings.ACTION_INPUT_METHOD_SETTINGS)
        enableIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(enableIntent)
        Handler(Looper.getMainLooper()).postDelayed({
            val intent = Intent(applicationContext, KeyBoardBottomEducationActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            if (intent.resolveActivity(packageManager) != null) {
                startActivity(intent)
            }
        }, 500)
        checkEnabledHandler.postDelayed(checkEnabled, 300)
    }

    private fun showIMEChooser() {
        showImeChooserCalled = true
        val imeManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imeManager.showInputMethodPicker()
    }

    private fun onStatusChange(
        old: IMEInstallStatus,
        aNew: IMEInstallStatus
    ) {
        if (old == IMEInstallStatus.NONE && aNew == IMEInstallStatus.NONE)
            DialogUtil.showKeyboardEnableBackDialog(this, object : DialogUtil.DialogButtonClickListener {
                override fun onPositiveButtonClick() {
                    launchEnableKeyboardPage()
                }

                override fun onNegativeButtonClick() {
                    onStatusChange(IMEInstallStatus.NONE)
                }

            })
        if (old == IMEInstallStatus.ENABLED && aNew == IMEInstallStatus.ENABLED) {
            onStatusChange(IMEInstallStatus.ENABLED)
        }
        if (aNew == IMEInstallStatus.SELECTED) {
            onStatusChange(IMEInstallStatus.SELECTED)
        }
    }

    protected open fun onStatusChange(imeInstallStatus: IMEInstallStatus) {
        // Left emtpy for sub-classes to handle
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == this.requestCode)
            onUserSetup()
    }

    protected open fun onUserSetup() {

    }

    protected fun isKeyboardOnboardingInProgress(): Boolean {
        return shouldShowImeSwitcher && (!isImeSwitcherShown || showImeChooserCalled)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (shouldShowImeSwitcher && !isImeSwitcherShown && hasFocus) {
            checkEnabledHandler.post(this::showIMEChooser)
            isImeSwitcherShown = true
        }
        if (showImeChooserCalled && hasFocus) {
            val status = getStatus(this)
            onStatusChange(this.imeInstallStatus, status)
            this.imeInstallStatus = status
            showImeChooserCalled = false
        }
    }

}