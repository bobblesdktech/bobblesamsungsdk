package com.touchtalent.bobblesdk.sample.samsung.services

import android.annotation.SuppressLint
import android.content.ClipDescription
import android.content.Intent
import android.graphics.Rect
import android.graphics.Region
import android.inputmethodservice.InputMethodService
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup.*
import android.view.inputmethod.*
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.inputmethod.EditorInfoCompat
import androidx.core.view.inputmethod.InputConnectionCompat
import androidx.core.view.inputmethod.InputContentInfoCompat
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager
import com.beloo.widget.chipslayoutmanager.SpacingItemDecoration
import com.touchtalent.bobble.content.BobbleContentView
import com.touchtalent.bobble.core.BobbleInputMethodDelegate
import com.touchtalent.bobble.core.BobbleSDK
import com.touchtalent.bobble.core.interfaces.prompts.AutoPromptService
import com.touchtalent.bobble.core.interfaces.prompts.PromptType
import com.touchtalent.bobblesdk.sample.samsung.adapter.EmojiAdapter
import com.touchtalent.bobblesdk.sample.samsung.adapter.TextAdapter
import com.touchtalent.bobblesdk.sample.samsung.databinding.ItemTextBinding
import com.touchtalent.bobblesdk.sample.samsung.databinding.SampleContentDrawerBinding
import com.touchtalent.bobblesdk.sample.samsung.databinding.SampleContentPanelBinding
import com.touchtalent.bobblesdk.sample.samsung.databinding.SampleKeyboardBinding
import com.touchtalent.bobblesdk.sample.samsung.dialog.DialogLanguageView
import com.touchtalent.bobblesdk.sample.samsung.model.LanguageModel
import com.touchtalent.bobblesdk.sample.samsung.util.*
import com.touchtalent.bobblesdk.sdk_content_drawer.BobbleContentDrawerView
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

/**
 * Sample Keyboard to demonstrate usage of BobbleContentSDK in any keyboard
 */
class SampleKeyboard : InputMethodService() {

    companion object {
        const val KEY_CODE_DELETE = -1
        const val KEY_CODE_ACTION = -2
        const val KEY_CODE_LANGUAGE_SWITCH = -3
    }

    private var inputViewBinding: SampleKeyboardBinding? = null
    private var supportedMimeTypes: List<String> = emptyList()

    private var sampleContentPanelBinding: SampleContentPanelBinding? = null
    private var sampleContentDrawerBinding: SampleContentDrawerBinding? = null
    private var languageKey: TextView? = null

    private var touchableRegion: (() -> Region)? = null

    private var customInputConnection: InputConnection? = null
    private var customEditorInfo: EditorInfo? = null

    private var kbOpenCloseScope = MainScope()

    private var isContentDrawerShowing = false

    private val textManager = TextManager(this)

    private var currentLanguageModel = LanguageModel.KOREAN
    private var currentDownloadLocales: MutableSet<String> =
        mutableSetOf(LanguageModel.KOREAN.locale)

    /**
     * Create BobbleDelegate to access features to personalise content and to detect intents.
     * Certain IME callbacks like [onCreate], [onDestroy], [onFinishInput], [onFinishInputView],
     * [onStartInput], [onStartInputView], [BobbleInputMethodDelegate.processText] needs to be passed.
     * It is preferred to call these methods right after super calls.
     *
     * These callbacks are used to bind to the IME lifecycle and facilitate personalization tools to
     * work optimally. For e.g - [onStartInputView] lets us know which app keyboard is open and
     * whether or not content drawer should be opened here. (Content Drawer is opened only on messaging apps)
     */
    private var bobbleDelegate: BobbleInputMethodDelegate =
        BobbleSDK.newInputMethodServiceDelegate(this)

    /**
     * Retrieve an instance of auto-prompt service which will provide callbacks to listen to moments
     * when content drawer can be opened
     */
    private var autoPromptProcessor = bobbleDelegate.getService(AutoPromptService::class.java)

    private val languages =
        listOf(
            LanguageModel.ENGLISH,
            LanguageModel.HINDI,
            LanguageModel.GUJARATI,
            LanguageModel.BENGALI,
            LanguageModel.MALAYALAM,
            LanguageModel.MARATHI,
            LanguageModel.TELUGU,
            LanguageModel.TAMIL,
            LanguageModel.PUNJABI,
            LanguageModel.FRENCH,
            LanguageModel.KOREAN
        )

    override fun onCreate() {
        super.onCreate()
        // Pass onCreate to delegate
        bobbleDelegate.onCreate()
        textManager.setTextListener {
            // Pass complete typed text information, whenever the text on the input field changes.
            bobbleDelegate.processText(it)
        }
        updateLanguages()
    }

    private fun updateLanguages() {
        // Pass locales of all languages that the user has downloaded. This helps us to download
        // required models and metadata for personalisation of content and intent detection from
        // typed text
        BobbleSDK.setCurrentDownloadedLocales(currentDownloadLocales.toList())
        // Pass locale of current keyboard language. This is responsible for controlling language
        // of all features of our SDK
        BobbleSDK.setCurrentLocale(currentLanguageModel.locale)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateInputView(): View {
        return SampleKeyboardBinding.inflate(layoutInflater).also {
            inputViewBinding = it
            it.keywords.addItemDecoration(SpacingItemDecoration(5.dp, 7.dp))
            it.keywords.layoutManager = ChipsLayoutManager.newBuilder(this)
                .build()
            setupListeners(it)
            val keys1 = intArrayOf(
                'q'.code,
                'w'.code,
                'e'.code,
                'r'.code,
                't'.code,
                'y'.code,
                'u'.code,
                'i'.code,
                'o'.code,
                'p'.code
            )
            val keys2 = intArrayOf(
                'a'.code,
                's'.code,
                'd'.code,
                'f'.code,
                'g'.code,
                'h'.code,
                'j'.code,
                'k'.code,
                'l'.code
            )
            val keys3 = intArrayOf(
                'z'.code,
                'x'.code,
                'c'.code,
                'v'.code,
                'b'.code,
                'n'.code,
                'm'.code,
                KEY_CODE_DELETE
            )
            val keys4 = intArrayOf(KEY_CODE_LANGUAGE_SWITCH, ' '.code, KEY_CODE_ACTION)
            it.keys1.setKeys(keys1)
            it.keys2.setKeys(keys2)
            it.keys3.setKeys(keys3)
            it.keys4.setKeys(keys4)
            it.emojiBar.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
            it.emojiBar.adapter = EmojiAdapter(EMOJI_LIST, this::inputText)
            it.backspace.setOnTouchListener(DeleteKeyOnTouchListener {
                textManager.deleteLastChar()
            })
            updateKeyboardLayout()
        }.root
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun LinearLayout.setKeys(keys: IntArray) {
        for (key in keys) {
            val binding = ItemTextBinding.inflate(
                layoutInflater,
                this,
                true
            )
            binding.root.updateLayoutParams<LinearLayout.LayoutParams> {
                weight = when (key) {
                    KEY_CODE_DELETE -> 2f
                    KEY_CODE_ACTION -> 2.5f
                    KEY_CODE_LANGUAGE_SWITCH -> 2f
                    ' '.code -> 6f
                    else -> 1f
                }
            }
            when (key) {
                KEY_CODE_DELETE -> {
                    binding.root.text = "back"
                    binding.root.setOnTouchListener(DeleteKeyOnTouchListener {
                        textManager.deleteLastChar()
                    })
                }
                KEY_CODE_ACTION -> {
                    binding.root.text = "action"
                    binding.root.setOnClickListener {
                        currentInputEditorInfo?.let { editorInfo ->
                            val actionId = getImeOptionsActionIdFromEditorInfo(editorInfo)
                            currentInputConnection?.performEditorAction(actionId)
                        }
                    }
                }
                KEY_CODE_LANGUAGE_SWITCH -> {
                    languageKey = binding.root
                    binding.root.text = currentLanguageModel.shortName
                    binding.root.setOnClickListener {
                        openLanguageManager()
                    }
                }
                ' '.code -> {
                    binding.root.text = "space"
                    binding.root.setOnClickListener {
                        inputText(key.toChar().toString())
                    }
                }
                else -> {
                    binding.root.text = key.toChar().toString()
                    binding.root.setOnClickListener {
                        inputText(key.toChar().toString())
                    }
                }
            }
        }
    }

    private fun openLanguageManager() {
        val dialogView = DialogLanguageView(this)
        addToFreeArea(dialogView, dialogView.getTouchableRegion()) {
            height = LayoutParams.MATCH_PARENT
            width = LayoutParams.MATCH_PARENT
            setMargins(20.dp, 20.dp, 20.dp, 20.dp)
        }
        dialogView.update(languages, currentLanguageModel, currentDownloadLocales)
        dialogView.downloadListener = { locale ->
            currentDownloadLocales.add(locale)
            updateLanguages()
            dialogView.update(languages, currentLanguageModel, currentDownloadLocales)
        }
        dialogView.deleteListener = { locale ->
            currentDownloadLocales.remove(locale)
            updateLanguages()
            dialogView.update(languages, currentLanguageModel, currentDownloadLocales)
        }
        dialogView.currentLanguageChanger = {
            currentLanguageModel = it
            languageKey?.text = currentLanguageModel.shortName
            updateKeyboardLayout()
            updateLanguages()
            dialogView.update(languages, currentLanguageModel, currentDownloadLocales)
        }
        dialogView.closeListener = { removeFreeArea() }
    }

    private fun updateKeyboardLayout() {
        val keywords = currentLanguageModel.keywords
        val inputBinding = inputViewBinding ?: return
        if (keywords.isEmpty()) {
            inputBinding.keys1.visibility = VISIBLE
            inputBinding.keys2.visibility = VISIBLE
            inputBinding.keys3.visibility = VISIBLE
            inputBinding.keywords.visibility = INVISIBLE
            inputBinding.backspace.visibility = INVISIBLE
        } else {
            inputBinding.keys1.visibility = INVISIBLE
            inputBinding.keys2.visibility = INVISIBLE
            inputBinding.keys3.visibility = INVISIBLE
            inputBinding.keywords.visibility = VISIBLE
            inputBinding.backspace.visibility = VISIBLE
            inputBinding.keywords.adapter = TextAdapter(keywords) {
                inputText(it)
            }
        }
    }

    override fun setInputView(view: View?) {
        super.setInputView(view)
        updateFullscreenMode()
    }

    private fun setupListeners(binding: SampleKeyboardBinding) {
        binding.contentIcon.setOnClickListener {
            openContentPanel()
        }
    }

    private fun canDetectPrompt(): Boolean {
        return supportedMimeTypes.isNotEmpty()
    }

    private fun View.getTouchableRegion(): () -> Region {
        return {
            val rect = Rect(
                x.toInt(), y.toInt(),
                (x + width).toInt(),
                (y + height).toInt()
            )
            Region().apply { union(rect) }
        }
    }

    private fun openContentDrawer(bundle: Bundle) {
        sampleContentDrawerBinding?.let {
            // If content drawer is already visible and needs to be updated for new prompts, update
            // needs to be called
            it.bobbleContentDrawer.update(getCurrentText(), bundle)
            return
        }
        /**
         * The view supports light or mode which is dependent on the [android.content.Context] being
         * used to inflate it. Modified theme could be used if theme needs to be override. Here light
         * theme is overridden for all modes
         */
        val context = changeTheme(false)
        val drawerBinding = SampleContentDrawerBinding.inflate(LayoutInflater.from(context))
        sampleContentDrawerBinding = drawerBinding
        // Content drawer reference from inflated view
        val drawer: BobbleContentDrawerView = drawerBinding.bobbleContentDrawer
        /**
         * Call load to initiate search. current typed text, supported MIME types and bundle
         * received from auto-prompts must be passed here
         */
        drawer.load(getCurrentText(), supportedMimeTypes, bundle)
        /**
         * Set share listener for content sharing. Uri and MIME type are provided.
         * Uri is a content:// Uri created with FileProvider with necessary permissions
         */
        drawer.setShareListener { uri, mime ->
            removeFreeArea()
            shareUri(uri, mime)
        }
        addToFreeArea(drawerBinding.root, drawerBinding.root.getTouchableRegion()) {
            height = 80.dp
            width = LayoutParams.MATCH_PARENT
            marginStart = 10.dp
            marginEnd = 10.dp
            bottomMargin = 55.dp
            gravity = Gravity.BOTTOM
        }
    }

    private inline fun addToFreeArea(
        view: View,
        noinline touchableRegion: () -> Region,
        updateLayoutParams: FrameLayout.LayoutParams.() -> Unit,
    ) {
        this.touchableRegion = touchableRegion
        val binding = inputViewBinding ?: return
        binding.freeArea.addView(view)
        view.updateLayoutParams<FrameLayout.LayoutParams> {
            updateLayoutParams(this)
        }
    }

    private fun removeFreeArea() {
        touchableRegion = null
        isContentDrawerShowing = false
        val binding = inputViewBinding ?: return
        binding.freeArea.removeAllViews()
        sampleContentDrawerBinding = null
    }

    @Suppress("OVERRIDE_DEPRECATION")
    override fun onViewClicked(focusChanged: Boolean) {
        super.onViewClicked(focusChanged)
        removeCustomView(true)
        removeTopView()
        removeFreeArea()
    }

    override fun updateFullscreenMode() {
        val window = window.window ?: return
        updateLayoutHeightOf(window, LayoutParams.MATCH_PARENT)
        // This method may be called before {@link #setInputView(View)}.
        val binding = inputViewBinding ?: return
        val layoutHeight =
            if (isFullscreenMode) LayoutParams.WRAP_CONTENT else LayoutParams.MATCH_PARENT
        val inputArea = window.findViewById<View>(android.R.id.inputArea)
        updateLayoutHeightOf(inputArea, layoutHeight)
        updateLayoutHeightOf(binding.root, layoutHeight)
        super.updateFullscreenMode()
    }

    override fun onComputeInsets(outInsets: Insets) {
        super.onComputeInsets(outInsets)
        val binding = inputViewBinding ?: return
        val keyboardTop = binding.keyboardTop.y.toInt()
        val region = Region()
        val keyboardRect = Rect(0, keyboardTop, binding.root.width, binding.root.height)
        region.union(keyboardRect)
        touchableRegion?.let { region.op(it(), Region.Op.UNION) }
        outInsets.touchableInsets = Insets.TOUCHABLE_INSETS_REGION
        outInsets.touchableRegion.set(region)
        if (keyboardTop > 0) {
            outInsets.contentTopInsets = keyboardTop
            outInsets.visibleTopInsets = keyboardTop
        }
    }

    override fun onStartInput(attribute: EditorInfo?, restarting: Boolean) {
        super.onStartInput(attribute, restarting)
        bobbleDelegate.onStartInput(attribute, restarting)
    }

    override fun onStartInputView(info: EditorInfo?, restarting: Boolean) {
        super.onStartInputView(info, restarting)
        bobbleDelegate.onStartInputView(info, restarting)
        textManager.onStart(getCurrentText())
        updateFullscreenMode()
        val binding = inputViewBinding ?: return
        supportedMimeTypes =
            info?.let { EditorInfoCompat.getContentMimeTypes(it) }?.toList() ?: emptyList()
        val canShowContentIcon = supportedMimeTypes.isNotEmpty()
        binding.contentIcon.isVisible = canShowContentIcon
        binding.contentTooltip.isVisible = canShowContentIcon
    }

    private fun getCurrentText(): String {
        val tmpExtractedText: ExtractedText? =
            currentInputConnection?.getExtractedText(ExtractedTextRequest(), 0)
        return tmpExtractedText?.text?.toString() ?: ""
    }

    override fun onFinishInputView(finishingInput: Boolean) {
        super.onFinishInputView(finishingInput)
        bobbleDelegate.onFinishInputView(finishingInput)
        supportedMimeTypes = emptyList()
        textManager.onFinish()
    }

    override fun onFinishInput() {
        super.onFinishInput()
        bobbleDelegate.onFinishInput()
    }

    /**
     * Sample integration of Content Panel integration.
     *
     */
    private fun openContentPanel() {
        val contentPanel =
            sampleContentPanelBinding ?: SampleContentPanelBinding.inflate(layoutInflater).also {
                sampleContentPanelBinding = it
                // Instance of bobbleContentView from binding
                val contentView: BobbleContentView = it.contentView
                /**
                 * Prevent auto-destroy of BobbleContentView. This is done to prevent clearing out
                 * resources when keyboard layout in shown upon content search, since same instance
                 * must be retained
                 *
                 * P.S:- The view must be carefully stored and [BobbleContentView.destroy] called
                 * to clear out resources and prevent memory leak
                 */
                contentView.canDestroy = false
                /**
                 * Call [BobbleContentView.load] to initiate content loading. List of supported MIME
                 * types (extracted from editorInfo), current text and package name needs to be provided
                 */
                contentView.load(
                    supportedMimeTypes,
                    getCurrentText()
                )
                /**
                 * Since, keyboard input is a complicated mechanism, these callbacks facilitate
                 * taking input for the search functionality in content panel
                 *
                 * onViewCreatedListener -> Called with a custom view - searchView (containing
                 * search suggestions, need to be added on top of keyboard) and EditText - editText -
                 * which should be used to transfer keyboard's focus on it to be able to receive inputs
                 * Upon receiving this callback, the keyboard view must be restored and content panel
                 * view must be cached
                 *
                 * onRevertToStickerView -> Called when back button is pressed on searchView or
                 * Search action button is pressed. Upon receiving this callback, the content panel
                 * view must be restored
                 */
                contentView.setSearchListener(
                    onViewCreatedListener = { searchView, editText ->
                        removeCustomView(false)
                        addTopView(searchView)
                        changeInputConnection(editText)
                    },
                    onRevertToStickerView = {
                        sampleContentPanelBinding?.root?.let { contentView ->
                            addCustomView(contentView)
                        }
                    }
                )
                /**
                 * Set share listener for content sharing. Uri and MIME type are provided.
                 * Uri is a content:// Uri created with FileProvider with necessary permissions
                 */
                contentView.setShareListener { uri, mime ->
                    shareUri(uri, mime)
                }
            }
        removeFreeArea()
        addCustomView(contentPanel.root)
    }

    private fun shareUri(uri: Uri, mime: String?) {
        val inputConnection = currentInputConnection ?: return
        val editorInfo = currentInputEditorInfo ?: return

        val flag = if (Build.VERSION.SDK_INT >= 25)
            InputConnectionCompat.INPUT_CONTENT_GRANT_READ_URI_PERMISSION
        else {
            grantUriPermission(
                editorInfo.packageName, uri,
                Intent.FLAG_GRANT_READ_URI_PERMISSION
            )
            0
        }
        val inputContentInfo =
            InputContentInfoCompat(uri, ClipDescription(null, arrayOf(mime)), null)
        InputConnectionCompat.commitContent(
            inputConnection,
            editorInfo,
            inputContentInfo,
            flag,
            null
        )
    }

    private fun changeInputConnection(editText: EditText) {
        customInputConnection = CustomInputConnection(editText)
        customEditorInfo = EditorInfo()
            .apply {
                inputType = editText.inputType
                imeOptions = editText.imeOptions
                actionId = editText.imeActionId
                initialSelEnd = editText.selectionEnd
                initialSelStart = editText.selectionStart
                packageName = this@SampleKeyboard.packageName
            }
    }

    private fun resetCustomInputConnection() {
        customInputConnection = null
        customEditorInfo = null
    }

    private fun addTopView(view: View, height: Int = LayoutParams.MATCH_PARENT) {
        val binding = inputViewBinding ?: return
        binding.topBarContainer.isVisible = true
        binding.topBarContainer.addView(view)
        view.updateLayoutParams {
            this.height = height
            width = LayoutParams.MATCH_PARENT
        }
    }

    private fun removeTopView() {
        resetCustomInputConnection()
        val binding = inputViewBinding ?: return
        binding.topBarContainer.isVisible = false
        binding.topBarContainer.removeAllViews()
    }

    private fun addCustomView(view: View) {
        removeTopView()
        if (view.parent != null)
            return
        val inputViewBinding = inputViewBinding ?: return
        inputViewBinding.customViewContainer.isVisible = true
        inputViewBinding.customViewContainer.addView(view)
        view.updateLayoutParams {
            height = LayoutParams.MATCH_PARENT
            width = LayoutParams.MATCH_PARENT
        }
    }

    private fun removeCustomView(canDestroy: Boolean) {
        resetCustomInputConnection()
        val inputViewBinding = inputViewBinding ?: return
        inputViewBinding.customViewContainer.isVisible = false
        inputViewBinding.customViewContainer.removeAllViews()
        /**
         * [BobbleContentView.destroy] must be called on [BobbleContentView] to clear the resources
         */
        if (canDestroy) {
            sampleContentPanelBinding?.contentView?.destroy()
            sampleContentPanelBinding = null
        }
    }

    override fun onWindowShown() {
        super.onWindowShown()
        /**
         * Listen to auto prompt processor events. These events are generated in response to user
         * typed text passed in [BobbleInputMethodDelegate.processText].
         *
         */
        if (canDetectPrompt()) {
            kbOpenCloseScope.launch {
                autoPromptProcessor?.getPromptFlow()?.collectLatest { promptResponse ->
                    // Open content drawer. The bundle here should be passed for accurate content
                    // landing
                    if (promptResponse.type == PromptType.CONTENT_DRAWER) {
                        openContentDrawer(promptResponse.bundle)
                    }
                }
            }
        }
    }

    override fun onWindowHidden() {
        super.onWindowHidden()
        resetCustomInputConnection()
        kbOpenCloseScope.coroutineContext.cancelChildren()
        removeCustomView(true)
        removeTopView()
        removeFreeArea()
    }

    private fun inputText(input: String) {
        textManager.commitText(input)
    }

    override fun getCurrentInputConnection(): InputConnection? {
        return customInputConnection ?: super.getCurrentInputConnection()
    }

    override fun getCurrentInputEditorInfo(): EditorInfo? {
        return customEditorInfo ?: super.getCurrentInputEditorInfo()
    }

    private fun getImeOptionsActionIdFromEditorInfo(editorInfo: EditorInfo): Int {
        return if (editorInfo.imeOptions and EditorInfo.IME_FLAG_NO_ENTER_ACTION != 0) {
            EditorInfo.IME_ACTION_NONE
        } else {
            // Note: this is different from editorInfo.actionId, hence "ImeOptionsActionId"
            editorInfo.imeOptions and EditorInfo.IME_MASK_ACTION
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        bobbleDelegate.onDestroy()
    }
}