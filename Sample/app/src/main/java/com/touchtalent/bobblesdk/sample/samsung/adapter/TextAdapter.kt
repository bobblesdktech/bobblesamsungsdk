package com.touchtalent.bobblesdk.sample.samsung.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.touchtalent.bobblesdk.sample.samsung.databinding.ItemTextAdapterBinding

class TextAdapter(private val keywords: List<String>, private val inputListener: (String) -> Unit) :
    RecyclerView.Adapter<TextAdapter.TextViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TextViewHolder {
        val binding = ItemTextAdapterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TextViewHolder(binding)
    }

    override fun getItemCount() = keywords.size

    override fun onBindViewHolder(holder: TextViewHolder, position: Int) {
        holder.binding.root.text = keywords[position]
        holder.binding.root.setOnClickListener {
            inputListener.invoke(keywords[holder.bindingAdapterPosition])
        }
    }

    class TextViewHolder(val binding: ItemTextAdapterBinding) :
        RecyclerView.ViewHolder(binding.root)
}
