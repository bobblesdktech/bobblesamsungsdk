package com.touchtalent.bobblesdk.sample.samsung.util

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.CountDownTimer
import android.view.MotionEvent
import android.view.View
import java.util.concurrent.TimeUnit


class DeleteKeyOnTouchListener(private var onBackSpaceEvent: () -> Unit) :
    View.OnTouchListener {

    val mKeyRepeatStartTimeout: Long = 400
    val mKeyRepeatInterval: Long = 50


    private val mTimer: CountDownTimer
    private var mState = KEY_REPEAT_STATE_INITIALIZED
    private var mRepeatCount = 0

    init {
        mTimer = object : CountDownTimer(MAX_REPEAT_COUNT_TIME, mKeyRepeatInterval) {
            override fun onTick(millisUntilFinished: Long) {
                val elapsed = MAX_REPEAT_COUNT_TIME - millisUntilFinished
                if (elapsed < mKeyRepeatStartTimeout) {
                    return
                }
                onKeyRepeat()
            }

            override fun onFinish() {
                onKeyRepeat()
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(v: View, event: MotionEvent): Boolean {
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                onTouchDown(v)
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                val x = event.x
                val y = event.y
                if (x < 0.0f || v.width < x || y < 0.0f || v.height < y) {
                    // Stop generating key events once the finger moves away from the view area.
                    onTouchCanceled(v)
                }
                return true
            }
            MotionEvent.ACTION_CANCEL, MotionEvent.ACTION_UP -> {
                onTouchUp(v)
                return true
            }
        }
        return false
    }

    private fun handleKeyDown() {

    }

    private fun handleKeyUp() {
        onBackSpaceEvent.invoke()
        ++mRepeatCount
    }

    private fun onTouchDown(v: View) {
        mTimer.cancel()
        mRepeatCount = 0
        handleKeyDown()
        v.isPressed = true
        mState = KEY_REPEAT_STATE_KEY_DOWN
        mTimer.start()
    }

    private fun onTouchUp(v: View) {
        mTimer.cancel()
        if (mState == KEY_REPEAT_STATE_KEY_DOWN) {
            handleKeyUp()
        }
        v.isPressed = false
        mState = KEY_REPEAT_STATE_INITIALIZED
    }

    private fun onTouchCanceled(v: View) {
        mTimer.cancel()
        v.setBackgroundColor(Color.TRANSPARENT)
        mState = KEY_REPEAT_STATE_INITIALIZED
    }

    // Called by {@link #mTimer} in the UI thread as an auto key-repeat signal.
    fun onKeyRepeat() {
        when (mState) {
            KEY_REPEAT_STATE_INITIALIZED -> {}
            KEY_REPEAT_STATE_KEY_DOWN -> {
                // Do not call {@link #handleKeyDown} here because it has already been called
                // in {@link #onTouchDown}.
                handleKeyUp()
                mState = KEY_REPEAT_STATE_KEY_REPEAT
            }
            KEY_REPEAT_STATE_KEY_REPEAT -> {
                handleKeyDown()
                handleKeyUp()
            }
        }
    }

    companion object {
        val MAX_REPEAT_COUNT_TIME = TimeUnit.SECONDS.toMillis(30)

        /**
         * Key-repeat state.
         */
        private const val KEY_REPEAT_STATE_INITIALIZED = 0

        // The key is touched but auto key-repeat is not started yet.
        private const val KEY_REPEAT_STATE_KEY_DOWN = 1

        // At least one key-repeat event has already been triggered and the key is not released.
        private const val KEY_REPEAT_STATE_KEY_REPEAT = 2
    }
}