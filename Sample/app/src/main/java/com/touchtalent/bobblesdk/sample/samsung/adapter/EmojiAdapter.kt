package com.touchtalent.bobblesdk.sample.samsung.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.touchtalent.bobblesdk.sample.samsung.databinding.ItemEmojiBinding

class EmojiAdapter(private val emojis: List<String>, private val inputListener: (String) -> Unit) :
    RecyclerView.Adapter<EmojiAdapter.EmojiViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmojiViewHolder {
        val binding = ItemEmojiBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return EmojiViewHolder(binding)
    }

    override fun getItemCount() = emojis.size

    override fun onBindViewHolder(holder: EmojiViewHolder, position: Int) {
        holder.binding.root.text = emojis[position]
        holder.binding.root.setOnClickListener {
            inputListener.invoke(emojis[holder.bindingAdapterPosition])
        }
    }

    class EmojiViewHolder(val binding: ItemEmojiBinding) :
        RecyclerView.ViewHolder(binding.root)
}
