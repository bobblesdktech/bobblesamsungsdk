package com.touchtalent.bobblesdk.sample.samsung.dialog

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.touchtalent.bobblesdk.sample.samsung.R
import com.touchtalent.bobblesdk.sample.samsung.adapter.LanguageViewAdapter
import com.touchtalent.bobblesdk.sample.samsung.databinding.DialogLanguageBinding
import com.touchtalent.bobblesdk.sample.samsung.model.LanguageModel

class DialogLanguageView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : ConstraintLayout(context, attrs) {
    private val binding = DialogLanguageBinding.inflate(LayoutInflater.from(context), this)

    var downloadListener: ((String) -> Unit)? = null
    var deleteListener: ((String) -> Unit)? = null
    var currentLanguageChanger: ((LanguageModel) -> Unit)? = null
    var closeListener: (() -> Unit)? = null

    private val adapter =
        LanguageViewAdapter(
            { downloadListener?.invoke(it) },
            { deleteListener?.invoke(it) },
            { currentLanguageChanger?.invoke(it) }
        )

    init {
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        binding.recyclerView.adapter = adapter
        binding.cancel.setOnClickListener {
            closeListener?.invoke()
        }
        setBackgroundResource(R.drawable.background_dialog_language_view)
    }

    fun update(
        languages: List<LanguageModel>,
        currentLanguage: LanguageModel,
        locales: Set<String>
    ) {
        adapter.update(languages, currentLanguage, locales)
    }
}