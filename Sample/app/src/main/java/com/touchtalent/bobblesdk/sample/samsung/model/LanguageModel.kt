package com.touchtalent.bobblesdk.sample.samsung.model

data class LanguageModel(
    val locale: String,
    val longName: String,
    val shortName: String,
    val keywords: List<String>
) {
    companion object {
        val ENGLISH = LanguageModel("en_IN", "English (India)", "En", emptyList())
        val HINDI = LanguageModel(
            "hi_IN",
            "हिंदी (India)",
            "Hi",
            listOf(
                "शुभ प्रभात",
                "सुप्रभात",
                "प्रभात",
                "नमस्कार",
                "शुभ रात्रि",
                "मुझे तुमसे प्यार है",
                "जन्मदिन की शुभकामनाएँ",
                "तुमसे प्यार है",
                "धन्यवाद",
                "मुझे कॉल करो",
                "तुम्हें प्यार करता हूं",
                "अल्लाह हाफिज",
                "खाना खा लिया",
                "सुबह",
                "रात",
                "जीएम",
                "क्षमा मांगना",
                "धन्यवाद",
                "व्यस्त",
                "बेब",
                "गुस्सा",
                "मीठी नींद आए",
                "अपना ध्यान रखना",
                "समय",
                "अलविदा",
                "बाबू",
                "महादेव",
                "जय श्री राम",
                "जय माता दी",
                "आपकी याद आ रही है"
            )
        )
        val GUJARATI = LanguageModel(
            "gu_IN",
            "ગુજરાતી (India)",
            "Gu",
            listOf(
                "સુપ્રભાત",
                "શુભ રાત્રી",
                "હું તને પ્રેમ કરું છુ",
                "જન્મદિવસ ની શુભકામના",
                "તમને પ્રેમ",
                "આભાર",
                "મને બોલાવો",
                "તને પ્રેમ કરો",
                "અલ્લાહ હાફિઝ",
                "ખાના ખા લિયા",
                "સવાર",
                "રાત",
                "ગ્રામ",
                "માફ કરશો",
                "આભાર",
                "વ્યસ્ત",
                "બેબી",
                "ગુસ્સો",
                "મીઠા સપના",
                "કાળજી રાખજો",
                "સમય",
                "બાય",
                "બાબુ",
                "મહાદેવ",
                "જય શ્રી રામ",
                "જય માતા દી",
                "તારી યાદ સતાવે છે"
            )
        )
        val BENGALI = LanguageModel(
            "bn_IN",
            "বাংলা (India)",
            "Bn",
            listOf(
                "সুপ্রভাত",
                "শুভ রাত্রি",
                "আমি তোমাকে ভালোবাসি",
                "শুভ জন্মদিন",
                "তোমাকে ভালোবাসি",
                "ধন্যবাদ",
                "আমাকে ডাকো",
                "তোমাকে ভালবাসি",
                "আল্লাহ হাফিজ",
                "খানা খা লিয়া",
                "সকাল",
                "রাত্রি",
                "জিএম",
                "দুঃখিত",
                "ধন্যবাদ",
                "ব্যস্ত",
                "বাবু",
                "রাগ",
                "মিষ্টি স্বপ্ন",
                "যত্ন নিবেন",
                "সময়",
                "বাই",
                "বাবু",
                "মহাদেব",
                "জয় শ্রী রাম",
                "জয় মাতা দি",
                "তোমাকে মিস করি"
            )
        )
        val MARATHI = LanguageModel(
            "mr_IN",
            "मराठी (India)",
            "Mr",
            listOf(
                "गुड मॉर्निंग",
                "शुभ रात्री",
                "मी तुझ्यावर प्रेम करतो",
                "वाढदिवसाच्या शुभेच्छा",
                "लव्ह यू",
                "थँक्यू",
                "मला कॉल",
                "लव्ह यू",
                "अल्लाह हाफिज",
                "खाना खा लिया",
                "सकाळ",
                "रात्री",
                "जीएम",
                "सॉरी",
                "धन्यवाद",
                "व्यस्त",
                "बाळ",
                "राग",
                "गोड स्वप्ने",
                "काळजी घे",
                "वेळ",
                "बाय",
                "बाबू",
                "महादेव",
                "जय श्री राम",
                "जय माता दी",
                "मिस यू"
            )
        )
        val MALAYALAM = LanguageModel(
            "ml_IN",
            "മലയാളം (India)",
            "Ml",
            listOf(
                "സുപ്രഭാതം",
                "ശുഭ രാത്രി",
                "ഞാൻ നിന്നെ സ്നേഹിക്കുന്നു",
                "ജന്മദിനാശംസകൾ",
                "എനിക്ക് നിന്നെ ഇഷ്ടം ആണ്",
                "നന്ദി",
                "എന്നെ വിളിക്കുക",
                "ലവ് യു",
                "അല്ലാഹു ഹാഫിസ്",
                "ഖാനാ ഖ ലിയ",
                "രാവിലെ",
                "രാത്രി",
                "ജിഎം",
                "ക്ഷമിക്കണം",
                "നന്ദി",
                "തിരക്ക്",
                "കുഞ്ഞേ",
                "കോപം",
                "മധുരസ്വപ്നങ്ങൾ",
                "ശ്രദ്ധപുലർത്തുക",
                "സമയം",
                "ബൈ",
                "ബാബു",
                "മഹാദേവ്",
                "ജയ് ശ്രീറാം",
                "ജയ് മാതാ ദി",
                "നിങ്ങളെ മിസ്സാകുന്നു"
            )
        )
        val TELUGU = LanguageModel(
            "te_IN",
            "తెలుగు (India)",
            "Te",
            listOf(
                "శుభోదయం",
                "శుభ రాత్రి",
                "నేను నిన్ను ప్రేమిస్తున్నాను",
                "పుట్టినరోజు శుభాకాంక్షలు",
                "ప్రేమిస్తున్నాను",
                "ధన్యవాదాలు",
                "నాకు ఫోన్ చెయ్",
                "నిన్ను ప్రేమిస్తున్నాను",
                "అల్లా హఫీజ్",
                "ఖానా ఖ లియా",
                "ఉదయం",
                "రాత్రి",
                "gm",
                "క్షమించండి",
                "ధన్యవాదాలు",
                "బిజీగా",
                "పసికందు",
                "కోపం",
                "మంచి కలలు",
                "జాగ్రత్త",
                "సమయం",
                "బై",
                "బాబు",
                "మహాదేవ్",
                "జై శ్రీ రామ్",
                "జై మాతా ది",
                "మిస్ యు"
            )
        )
        val TAMIL = LanguageModel(
            "ta_IN",
            "தமிழ் (India)",
            "Ta",
            listOf(
                "காலை வணக்கம்",
                "இனிய இரவு",
                "நான் உன்னை காதலிக்கிறேன்",
                "பிறந்தநாள் வாழ்த்துக்கள்",
                "உன்னை காதலிக்கிறேன்",
                "நன்றி",
                "என்னை அழையுங்கள்",
                "லவ் யூ",
                "அல்லாஹ் ஹபீஸ்",
                "கானா கா லியா",
                "காலை",
                "இரவு",
                "ஜிஎம்",
                "மன்னிக்கவும்",
                "நன்றி",
                "பரபரப்பு",
                "குழந்தை",
                "கோபம்",
                "இனிமையான கனவுகள்",
                "கவனிக்கவும்",
                "நேரம்",
                "வருகிறேன்",
                "பாபு",
                "மகாதேவ்",
                "ஜெய் ஸ்ரீ ராம்",
                "ஜெய் மாதா டி",
                "உன்னை இழக்கிறேன்"
            )
        )
        val PUNJABI = LanguageModel(
            "pa_IN",
            "ਪੰਜਾਬੀ (India)",
            "Pa",
            listOf(
                "ਸ਼ੁਭ ਸਵੇਰ",
                "ਸ਼ੁਭ ਰਾਤ",
                "ਆਈ ਲਵ ਯੂ",
                "ਜਨਮਦਿਨ ਮੁਬਾਰਕ",
                "ਲਵ ਯੂ",
                "ਥੈਂਕ ਯੂ",
                "ਮੈਨੂੰ ਕਾਲ",
                "ਲਵ ਯੂ",
                "ਅੱਲ੍ਹਾ ਹਾਫਿਜ਼",
                "ਖਾਨਾ ਖਾ ਲਿਆ",
                "ਸਵੇਰ",
                "ਰਾਤ",
                "ਗ੍ਰਾਮ",
                "ਮਾਫ਼ ਕਰਨਾ",
                "ਧੰਨਵਾਦ",
                "ਰੁਝੇ ਹੋਏ",
                "ਬੇਬੇ",
                "ਗੁੱਸੇ",
                "ਮਿੱਠੇ ਸੁਪਨੇ",
                "ਧਿਆਨ ਰੱਖੋ",
                "ਸਮਾਂ",
                "ਬਾਈ",
                "ਬਾਬੂ",
                "ਮਹਾਦੇਵ",
                "ਜੈ ਸ਼੍ਰੀ ਰਾਮ",
                "ਜੈ ਮਾਤਾ ਦੀ",
                "ਮਿਸ ਯੂ"
            )
        )

        val FRENCH = LanguageModel(
            "fr_FR",
            "Français (France)",
            "Fr",
            listOf(
                "bonjour",
                "bonne nuit",
                "Je vous aime",
                "joyeux anniversaire",
                "je t'aime",
                "merci",
                "appelez-moi",
                "je t'aime",
                "allah hafiz",
                "khana kha liya",
                "matin",
                "nuit",
                "gm",
                "Désolé",
                "merci",
                "occupé",
                "bébé",
                "en colère",
                "beaux rêves",
                "prends soin de toi",
                "temps",
                "au revoir",
                "babou",
                "mahadev",
                "jai shree bélier",
                "jai mata di",
                "tu me manques"
            )
        )
        val KOREAN = LanguageModel(
            "ko_KR",
            "한국인 (Korea)",
            "Ko",
            listOf(
                "행쇼",
                "ㅋㅋ",
                "꿀잼",
                "아름다운",
                "춥다",
                "가족",
                "학교",
                "주세요",
                "천만에요",
                "미안해요",
                "고맙습니다",
                "아침",
                "좋은 아침이에요",
                "안녕히 주무세요",
                "사랑해요",
                "생일 축하해요",
                "사랑해요",
                "감사합니다",
                "전화해",
                "사랑해",
                "알라 하 피즈",
                "카나 카 리야",
                "아침",
                "밤",
                "GM",
                "죄송합니다",
                "감사해요",
                "바쁘다",
                "아기",
                "화난",
                "좋은 꿈 꿔",
                "잘 지내세요",
                "시간",
                "안녕",
                "바부",
                "마하데브",
                "자이 슈리 램",
                "자이 마타 디",
                "보고 싶어요"
            )
        )
    }
}