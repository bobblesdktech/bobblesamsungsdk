package com.touchtalent.bobblesdk.sample.samsung.util

import android.content.Context
import android.content.res.Configuration
import android.view.ContextThemeWrapper
import androidx.annotation.StyleRes
import java.util.*

fun Context.changeTheme(isDarkTheme: Boolean) =
    Modifier(this).updateUiMode(isDarkTheme)
        .modify()


/**
 * Utility builder class to modify parameters of a context such as UI mode, locale, theme, etc
 * @param context Context to modify
 * @return New context instance with modified parameters
 */
class Modifier(private val context: Context) {
    private var nightMode: Boolean? = null
    private var locale: Locale? = null

    @StyleRes
    private var theme: Int? = null

    /**
     * Change light/dark mode of the context
     * @param nightMode true if dark mode is required, false otherwise
     * @return Same [Modifier] instance for chaining
     */
    fun updateUiMode(nightMode: Boolean): Modifier {
        this.nightMode = nightMode
        return this
    }

    /**
     * Change locale of the context
     * @param locale new locale to set
     * @return Same [Modifier] instance for chaining
     */
    fun updateLocale(locale: Locale): Modifier {
        this.locale = locale
        return this
    }

    /**
     * Add a theme/style to the existing context
     * @param theme style resource of the theme to add
     * @return Same [Modifier] instance for chaining
     */
    fun updateTheme(@StyleRes theme: Int): Modifier {
        this.theme = theme
        return this
    }

    /**
     * Get the modified context after applying changes
     */
    fun modify(): Context {
        val config = Configuration(context.resources.configuration)
        nightMode?.let {
            val mask =
                if (it) Configuration.UI_MODE_NIGHT_YES else Configuration.UI_MODE_NIGHT_NO
            config.uiMode = config.uiMode or Configuration.UI_MODE_NIGHT_MASK
            config.uiMode = config.uiMode and mask
        }
        locale?.let {
            config.setLocale(it)
        } ?: run {
            config.setLocale(context.resources.configuration.locale)
        }
        val modifiedContext = context.createConfigurationContext(config)
        theme?.let {
            return ContextThemeWrapper(modifiedContext, it)
        } ?: return modifiedContext
    }
}
