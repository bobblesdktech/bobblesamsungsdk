package com.touchtalent.bobblesdk.sample.samsung.adapter

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.touchtalent.bobblesdk.sample.samsung.R
import com.touchtalent.bobblesdk.sample.samsung.databinding.ItemLanguageBinding
import com.touchtalent.bobblesdk.sample.samsung.model.LanguageModel

class LanguageViewAdapter(
    private val downloadListener: (String) -> Unit,
    private val deleteListener: (String) -> Unit,
    private val currentLanguageChangeListener: (LanguageModel) -> Unit
) : RecyclerView.Adapter<LanguageViewAdapter.LanguageViewHolder>() {

    private var languages: List<LanguageModel> = emptyList()
    private var downloadedLocales: Set<String> = emptySet()
    private var currentLanguage: LanguageModel? = null

    class LanguageViewHolder(val binding: ItemLanguageBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LanguageViewHolder {
        return LanguageViewHolder(
            ItemLanguageBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun getItemCount() = languages.size

    override fun onBindViewHolder(holder: LanguageViewHolder, position: Int) {
        val languageModel = languages[position]
        holder.binding.title.text = languageModel.longName
        holder.binding.download.setImageResource(
            if (isDownloaded(languageModel))
                R.drawable.ic_delete
            else R.drawable.ic_download
        )
        holder.binding.root.setBackgroundColor(
            if (currentLanguage?.locale == languageModel.locale)
                Color.parseColor("#808080")
            else Color.TRANSPARENT
        )
        holder.binding.download.setOnClickListener {
            if (isDownloaded(languageModel))
                deleteListener.invoke(languageModel.locale)
            else downloadListener.invoke(languageModel.locale)
        }
        holder.binding.root.setOnClickListener {
            if (!isDownloaded(languageModel)) {
                Toast.makeText(
                    holder.binding.root.context,
                    "Download the language first",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }
            currentLanguageChangeListener.invoke(languageModel)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun update(
        languages: List<LanguageModel>,
        currentLanguage: LanguageModel,
        locales: Set<String>
    ) {
        this.downloadedLocales = locales
        this.languages = languages
        this.currentLanguage = currentLanguage
        notifyDataSetChanged()
    }

    private fun isDownloaded(languageModel: LanguageModel): Boolean {
        return downloadedLocales.contains(languageModel.locale)
    }
}