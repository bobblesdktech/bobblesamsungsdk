package com.touchtalent.bobblesdk.sample.samsung.util

import android.inputmethodservice.InputMethodService

class TextManager(private val parent: InputMethodService) {

    private val currentCommittedInput = StringBuilder()
    private var currentComposingText = ""

    private val currentText: String
        get() = currentCommittedInput.toString() + currentComposingText

    private var listener: ((String) -> Unit)? = null

    fun onStart(currentText: String) {
        currentCommittedInput.setLength(0)
        currentCommittedInput.append(currentText)
        listener?.invoke(currentText)
    }

    fun onFinish() {
        currentCommittedInput.setLength(0)
        currentComposingText = ""
    }

    fun deleteLastChar() {
        currentCommittedInput.setLength((currentCommittedInput.length - 1).coerceAtLeast(0))
        parent.currentInputConnection.deleteSurroundingText(1, 0)
        listener?.invoke(currentText)
    }

    fun setComposingText(text: String) {
        currentComposingText = text
        parent.currentInputConnection.setComposingText(text, 1)
        listener?.invoke(currentText)
    }

    fun finishComposing() {
        currentCommittedInput.append(currentComposingText)
        currentComposingText = ""
        parent.currentInputConnection.finishComposingText()
        listener?.invoke(currentText)
    }

    fun setTextListener(listener: (updatedText: String) -> Unit) {
        this.listener = listener
    }

    fun commitText(substring: String) {
        currentCommittedInput.append(substring)
        parent.currentInputConnection.commitText(substring, 1)
        listener?.invoke(currentText)
    }
}