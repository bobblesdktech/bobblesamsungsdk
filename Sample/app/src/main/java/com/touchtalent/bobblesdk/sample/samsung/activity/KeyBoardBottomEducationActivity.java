package com.touchtalent.bobblesdk.sample.samsung.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.touchtalent.bobblesdk.sample.samsung.R;

public class KeyBoardBottomEducationActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean mHasResume;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);

        View view = LayoutInflater.from(this).inflate(R.layout.enable_bobble_toast_popup, null, false);
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        int screenHeight = getResources().getDisplayMetrics().heightPixels;
        Window window = getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        window.getDecorView().setPadding(0, 0, 0, 0);
        wlp.gravity = Gravity.BOTTOM;
        wlp.width = screenWidth;
        view.measure(MeasureSpec.makeMeasureSpec(screenWidth, MeasureSpec.EXACTLY),
            MeasureSpec.makeMeasureSpec(screenHeight, MeasureSpec.AT_MOST));
        wlp.height = view.getMeasuredHeight();
        window.setAttributes(wlp);

        setContentView(R.layout.enable_bobble_toast_popup);
        TextView textView = findViewById(R.id.bobbleTitle);

        textView.setText(getResources().getString(R.string.bobble_ai_keyboard_is_safe_amp_secure,
            getResources().getString(R.string.app_name)));
        initContentView();
        new Handler(Looper.myLooper()).postDelayed(this::finish, 3000);
    }

    private void initContentView() {
        View rootView = findViewById(R.id.container);
        rootView.setOnClickListener(this);

        AppCompatImageView animationView = rootView.findViewById(R.id.animationView);
        AppCompatImageView lockIconView = rootView.findViewById(R.id.lockIconView);

        Glide.with(this)
            .asDrawable()
            .load(R.drawable.ic_lock_icon)
            .into(lockIconView);
        Glide.with(this)
            .asGif()
            .load(R.drawable.bobble_security)
            .placeholder(R.drawable.color_selector_drawable)
            .diskCacheStrategy(DiskCacheStrategy.DATA)
            .into(animationView);
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mHasResume) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, 1000);
        }
        mHasResume = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        finish();
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        OnboardingEventUtils.logEnableKBAnimationTap();
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        finish();
    }
}
