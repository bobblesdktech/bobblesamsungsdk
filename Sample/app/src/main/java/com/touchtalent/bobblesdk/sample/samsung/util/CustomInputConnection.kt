package com.touchtalent.bobblesdk.sample.samsung.util

import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.KeyEvent
import android.view.inputmethod.*
import android.widget.TextView

class CustomInputConnection(private val mTextView: TextView) :
    BaseInputConnection(mTextView, true) {
    // Keeps track of nested begin/end batch edit to ensure this connection always has a
    // balanced impact on its associated TextView.
    // A negative value means that this connection has been finished by the InputMethodManager.
    private var mBatchEditNesting = 0
    override fun getEditable(): Editable {
        val tv = mTextView
        return tv.editableText
    }

    override fun beginBatchEdit(): Boolean {
        synchronized(this) {
            if (mBatchEditNesting >= 0) {
                mTextView.beginBatchEdit()
                mBatchEditNesting++
                return true
            }
        }
        return false
    }

    override fun endBatchEdit(): Boolean {
        synchronized(this) {
            if (mBatchEditNesting > 0) {
                // When the connection is reset by the InputMethodManager and reportFinish
                // is called, some endBatchEdit calls may still be asynchronously received from the
                // IME. Do not take these into account, thus ensuring that this IC's final
                // contribution to mTextView's nested batch edit count is zero.
                mTextView.endBatchEdit()
                mBatchEditNesting--
                return true
            }
        }
        return false
    }

    override fun clearMetaKeyStates(states: Int): Boolean {
        val content = editable
        val kl = mTextView.keyListener
        if (kl != null) {
            try {
                kl.clearMetaKeyState(mTextView, content, states)
            } catch (e: AbstractMethodError) {
                // This is an old listener that doesn't implement the
                // new method.
            }
        }
        return true
    }

    override fun commitCompletion(text: CompletionInfo): Boolean {
        if (DEBUG) Log.v(TAG, "commitCompletion $text")
        mTextView.beginBatchEdit()
        mTextView.onCommitCompletion(text)
        mTextView.endBatchEdit()
        return true
    }

    /**
     * Calls the [TextView.onCommitCorrection] method of the associated TextView.
     */
    override fun commitCorrection(correctionInfo: CorrectionInfo): Boolean {
        if (DEBUG) Log.v(TAG, "commitCorrection$correctionInfo")
        mTextView.beginBatchEdit()
        mTextView.onCommitCorrection(correctionInfo)
        mTextView.endBatchEdit()
        return true
    }

    override fun performEditorAction(actionCode: Int): Boolean {
        if (DEBUG) Log.v(TAG, "performEditorAction $actionCode")
        mTextView.onEditorAction(actionCode)
        return true
    }

    override fun performContextMenuAction(id: Int): Boolean {
        if (DEBUG) Log.v(TAG, "performContextMenuAction $id")
        mTextView.beginBatchEdit()
        mTextView.onTextContextMenuItem(id)
        mTextView.endBatchEdit()
        return true
    }

    override fun getExtractedText(request: ExtractedTextRequest, flags: Int): ExtractedText? {
        val et = ExtractedText()
        if (mTextView.extractText(request, et)) {
            return et
        }
        return null
    }

    override fun performPrivateCommand(action: String, data: Bundle): Boolean {
        mTextView.onPrivateIMECommand(action, data)
        return true
    }

    override fun commitText(text: CharSequence, newCursorPosition: Int): Boolean {
        return super.commitText(text, newCursorPosition)
    }

    override fun sendKeyEvent(event: KeyEvent): Boolean {
        mTextView.dispatchKeyEvent(event)
        return super.sendKeyEvent(event)
    }

    companion object {
        private const val DEBUG = false
        private const val TAG = "CustomInputConnection"
    }
}