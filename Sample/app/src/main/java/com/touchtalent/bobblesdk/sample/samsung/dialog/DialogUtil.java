package com.touchtalent.bobblesdk.sample.samsung.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.touchtalent.bobblesdk.sample.samsung.R;

public final class DialogUtil {

    public static final String TAG = DialogUtil.class.getSimpleName();

    private DialogUtil() {
        // This class is not publicly instantiable.
    }

    public interface DialogButtonClickListener {
        void onPositiveButtonClick();

        void onNegativeButtonClick();
    }

    public static void showKeyboardEnableBackDialog(Context context, DialogButtonClickListener listener) {
        Dialog settingsSelectionDialog = new Dialog(context, R.style.PrivacyDialog);
        try {
            settingsSelectionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Window window = settingsSelectionDialog.getWindow();
        if (window == null) return;
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        settingsSelectionDialog.setCanceledOnTouchOutside(true);
        settingsSelectionDialog.setContentView(R.layout.dialog_enable_keyboard_back);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        Button buttonOK = settingsSelectionDialog.findViewById(R.id.okDialog);
        Button buttonCancel = settingsSelectionDialog.findViewById(R.id.cancelDialog);
        buttonOK.setOnClickListener(v -> {
            if (!buttonOK.isEnabled()) {
                return;
            }
            listener.onPositiveButtonClick();
            if (settingsSelectionDialog.isShowing()) {
                settingsSelectionDialog.dismiss();
            }
        });
        buttonCancel.setOnClickListener(v -> {
            listener.onNegativeButtonClick();
            if (settingsSelectionDialog.isShowing()) {
                settingsSelectionDialog.dismiss();
            }
        });
        settingsSelectionDialog.setCancelable(true);
        try {
            settingsSelectionDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
