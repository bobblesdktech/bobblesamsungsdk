 #  Bobble Auto Prompt SDK

**Bobble Auto Prompt SDK** provides intent detection from user typed text and suggests content to the user. This works in conjuction with `content-drawer` module to predict and suggest content to user on a realtime basis.

## <a name="implementation_steps"></a>Implementation Steps

- Add and initialise BobbleSDK Core in your project. Refer [here](README.md#setup) for steps.

- Add following dependency in your application module’s build.gradle.
```groovy
implementation 'com.touchtalent.bobblesdk:auto-prompt'
```

Sync your Gradle project to ensure that the dependency is downloaded by the build system.

##  Bobble Auto Prompt APIs:

### AutoPromptService

```AutoPromptService``` is a service of `BobbleInputMethodDelegate` that provides hooks to listen to events whenever a prompt/intent is detected

#### APIs
1. `BobbleInputMethodDelegate.getService(class:Class<T>)` - Call this function on bobbleInputMethodDelegate instance to retrieve instance of `AutoPromptService`:
```kotlin
private var bobbleDelegate = BobbleSDK.newInputMethodServiceDelegate(this)
private var autoPromptProcessor = bobbleDelegate.getService(AutoPromptService::class.java)
```                
1. `AutoPromptService.getPromptFlow()` - Get a `SharedFlow<PromptResponse>` to listen for events when a prompt/intent is detected. Currently only prompt for content-drawer is supported.

```kotlin
data class PromptResponse(
    val type: PromptType, // Describes the action that needs to be performed 
    val bundle: Bundle? // Contains metadata of the prompt information. The action that needs to be performed will consume this.
)
enum class PromptType {
    CONTENT_DRAWER
}
```
#### Example

```kotlin
override fun onWindowShown() {
    super.onWindowShown()
    if (canDetectPrompt()) {
        kbOpenCloseScope.launch {
            autoPromptProcessor?.getPromptFlow()?.collectLatest { promptResponse ->
                if (promptResponse.type == PromptType.CONTENT_DRAWER) {
                    openContentDrawer(promptResponse.bundle)
                }
            }
        }
    }
}
```