 #  Bobble Content Drawer SDK

Bobble Content Drawer SDK provides an all in one solution for your content requirements:
1. Stickers
2. Animated Stickers
3. Movie GIFs
4. PopText
5. BigMoji

The SDK hosts a massive repository with more than 100K+ content.

## <a name="implementation_steps"></a>Implementation Steps

- Add and initialise BobbleSDK Core in your project. Refer [here](README.md#setup) for steps.

- Add following dependency in your application module’s build.gradle.
```groovy
implementation 'com.touchtalent.bobblesdk:content-drawer'
```

Sync your Gradle project to ensure that the dependency is downloaded by the build system.

##  Bobble Content Drawer APIs:

### BobbleContentDrawerView
![](assets/bobble-content-drawer-view.jpg)

```BobbleContentDrawerView``` imports a complete view showing different formats of contents. The UI supports light and dark modes. This view offers more content formats and can be used to suggest content to user while typing. Features of content drawer:

- Display multiple content and capture user interactions with them.
- Content recommendation based on user typed text

#### i. Add custom view inside a XML layout of your Keyboard layout
```xml
<com.touchtalent.bobblesdk.sdk_content_drawer.BobbleContentDrawerView
    android:id="@+id/bobble_content_drawer"
    android:layout_width="match_parent"
    android:layout_height="match_parent" />
```
#### ii. APIs
1. `load(currentText:String?, supportedMimeTypes:List<String>, bundle:Bundle)` - Call this function to start content loading inside the View. Following data is required for content to be loaded:
   - **supportedMimeTypes** - List of supported MIME types (extracted from editorInfo). This is used to decide the type of content supported by the parent app.
   - **currentText** - Current text present in the input box. This is used to search for relevant content
   - **bundle** - Bundle received from `auto-prompt` module to relevant content to the prompt detected. Empty bundle if not required.
                
2. `setShareListener(listener:(uri:Uri, mime:String?) -> Unit)` - Set listener to listen for content sharing. URI and mime type are provided. URI is a `content://` Uri created with FileProvider with necessary permissions for sharing.

3. `update(newText: String, bundle: Bundle?)` - Function to update the content based on updated text.
   - **newText** - Updated text, according to which content must be shown
   - **bundle** - Updated bundle according to which content must be shown.

iii. Example

- sample_content_panel.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.cardview.widget.CardView xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    app:cardBackgroundColor="#FFFFFF"
    app:cardCornerRadius="5dp">

    <com.touchtalent.bobblesdk.sdk_content_drawer.BobbleContentDrawerView
        android:id="@+id/bobble_content_drawer"
        android:layout_width="match_parent"
        android:layout_height="match_parent" />
</androidx.cardview.widget.CardView>
```
- openContentDrawer()
```kotlin
    private fun openContentDrawer(bundle: Bundle) {
        sampleContentDrawerBinding?.let {
            // If content drawer is already visible and needs to be updated for new prompts, update
            // needs to be called
            it.bobbleContentDrawer.update(getCurrentText(), bundle)
            return
        }
        /**
         * The view supports light or mode which is dependent on the [android.content.Context] being
         * used to inflate it. Modified theme could be used if theme needs to be override. Here light
         * theme is overridden for all modes
         */
        val context = changeTheme(false)
        val drawerBinding = SampleContentDrawerBinding.inflate(LayoutInflater.from(context))
        sampleContentDrawerBinding = drawerBinding
        // Content drawer reference from inflated view
        val drawer: BobbleContentDrawerView = drawerBinding.bobbleContentDrawer
        drawer.load(getCurrentText(), supportedMimeTypes, bundle)
        drawer.setShareListener { uri, mime ->
            removeFreeArea()
            shareUri(uri, mime)
        }
        // Add it to free area available above keyboard
        addToFreeArea(drawerBinding.root, drawerBinding.root.getTouchableRegion()) {
            height = 80.dp
            width = LayoutParams.MATCH_PARENT
            marginStart = 10.dp
            marginEnd = 10.dp
            bottomMargin = 55.dp
            gravity = Gravity.BOTTOM
        }
    }
```